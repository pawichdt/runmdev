import NextApp from "next/app";
import React from "react";
import { ThemeProvider } from "@material-ui/core/styles";
import DefaultLayout from "../components/layouts/DefaultLayout";
import appBarTheme from "../src/appBarTheme";

export default class App extends NextApp {
  // remove it here
  componentDidMount() {
    const jssStyles = document.querySelector("#jss-server-side");
    if (jssStyles && jssStyles.parentNode)
      jssStyles.parentNode.removeChild(jssStyles);
  }
  render() {
    const { Component, pageProps } = this.props;
    return (
      <div>
        <Component {...pageProps} />
      </div>
    );
  }
}
