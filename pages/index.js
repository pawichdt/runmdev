import Head from "next/head";
import { Box, Button } from "@material-ui/core";
import DefaultLayout from "../components/layouts/DefaultLayout";
// import styles from '../styles/Home.module.css'

export default function Home() {
  return (
    <DefaultLayout>
      <div>Hello</div>
    </DefaultLayout>
  );
}
